%% BE_410_411 
% This script is used to 
% 1. estimate the maximum pressure exerted on the cell bed in a flask 
% bioreactor during centrifugation
% 2. simulate the liquid surface inside the flask bioreactor 
% author: Nhan Nguyen 
% date: 12/28/2022 

%% Parameters 
clc, close, clear

br_length = [0.065 0.1];
br_height = [0.02 0.03]; 
br_length = br_length(1);
height = height(1);
base   = 0.0508;
fraction_volume = 0.1; 
radius = 0.1; 
gravity = 9.81; 
alpha = 20; 
angular_speed = 5; 
rho = 1;

% %% Solving equations (12/31/2022)
% static = []; 
% spinning = [];
% % spinning_2 = [];
% for alpha = 1:45
%     for angular_speed = 0:6:1000/60*2*pi
%         % Static fluid
%         dy = sqrt(2*0.1*br_length*height/tand(alpha));
%         static_dy = dy;
%         static_dx = tand(alpha)*static_dy;
%         static = [static; alpha angular_speed static_dx static_dy];
%         
%         % Spinning fluid
%         phi = 90-alpha+atand(radius*angular_speed^2/gravity);
%         if phi <= 90
%             flag = 1;
%             spinning_dy = (tand(phi)*static_dx*static_dy)^(1/2);
%             spinning_dx = static_dx*static_dy/spinning_dy;
%             spinning_delta_dx = 0;
%             max_p_x = sqrt(spinning_dx^2 + (spinning_dx/tand(alpha))^2);
%             max_p_h = radius*angular_speed^2/gravity;
%             max_pressure = rho*(max_p_x*radius*angular_speed^2 + gravity*max_p_h);
%             spinning = [spinning; flag alpha angular_speed spinning_dx spinning_dy spinning_delta_dx max_pressure];
%         else
%             spinning_dy = static_dy;
%             spinning_delta_dx = tand(phi-90)*spinning_dy;
%             spinning_dx =   (static_dx - 1/2*spinning_delta_dx);
%             if spinning_dx >= 0
%                 flag = 2;
%                 max_p_h = (spinning_delta_dx + spinning_dx)/cosd(alpha); 
%                 max_p_x = tand(90-atand(radius*angular_speed^2/gravity))*max_p_h;
%                 max_pressure = rho*(max_p_x*radius*angular_speed^2 + gravity*max_p_h);
%                 spinning = [spinning; flag alpha angular_speed spinning_dx spinning_dy spinning_delta_dx max_pressure];
%             else 
%                 beta = atand(radius+angular_speed^2/gravity) - alpha; 
%                 spinning_dy = sqrt(static_dx*static_dy/tand(beta)); 
%                 spinning_dx = tand(beta)*spinning_dy;
%                 spinning_delta_dx = 0;
%                 flag = 3;
%                 max_p_h = spinning_dx/cosd(alpha);
%                 max_p_x = tand(90 - beta - alpha)*max_p_h;
%                 max_pressure = rho*(max_p_x*radius*angular_speed^2 + gravity*max_p_h);
%                 spinning = [spinning; flag alpha angular_speed spinning_dx spinning_dy spinning_delta_dx max_pressure];
%             end
%         end
%     end
% end
% % 
% % plot3(spinning(:,2),spinning(:,3),spinning(:,7), "b.")
% % xlabel(["","Angle"],'FontSize',15, "FontWeight","bold")
% % ylabel(["Angular velocity (rad/s)",""],'FontSize',15, "FontWeight","bold")
% % zlabel(["","Pressure (Pa)"],'FontSize',15, "FontWeight","bold")
% % set(gca, "FontSize", 12)
% % title(["","Pressure Maximum Pressure wrt. Angle and Speed"],'FontSize',15, "FontWeight","bold")
% % zlim([0 3])

% 
% %% Solving equations (01/30/2022)
% pressure = [];
% total = [];
% length_x = 0:0.01:br_length(1);
% alphas = 1:45;
% angular_speeds = 0:6:(1000/60*2*pi);
% for alpha = alphas
%     for angular_speed = angular_speeds
%         for dx = length_x
%             pressure = rho * radius * angular_speed^2 * (base + cosd(alpha) * dx);
%             total = [total; alpha angular_speed pressure];
%         end
%     end
% end
% 
% plot3(total(:,1),total(:,2),total(:,3), "b.")
% xlabel(["","Angle"],'FontSize',15, "FontWeight","bold")
% ylabel(["Angular velocity (rad/s)",""],'FontSize',15, "FontWeight","bold")
% zlabel(["","Pressure (Pa)"],'FontSize',15, "FontWeight","bold")
% title(["","Pressure Distribution of the Cell Bed"],'FontSize',15, "FontWeight","bold")
% set(gca, "FontSize", 12)

%% Solving for parallel fluid surface (liquid uniformity)
betas = [];
fluid_heights = [];
uniform_speed = [];
uniform_angle = [];
length_x = 0:0.001:br_length(1);
alphas = 10:0.5:45;
angular_speeds = 2:0.6:20;
for angular_speed = angular_speeds
    for alpha = alphas
        for dx = length_x
            radius = sqrt((dx*sind(alpha))^2 + (base + dx*cosd(alpha))^2);
            beta = atand(radius*angular_speed^2/gravity);
            fluid_heights = [fluid_heights; (base + dx*cosd(alpha))*tand(beta)];
            betas = [betas; beta];
            if abs(alpha - beta) < 1e-1
                uniform_speed = [uniform_speed; angular_speed/(2*pi)*60];
                uniform_angle = [uniform_angle; alpha];
                fprintf("Angle: %2.2f, Speed: %2.2f RPM, Location: %2.3f\n", alpha, angular_speed/(2*pi)*60, dx)
            end
        end
    end
end

plot(length_x, fluid_heights(3*length(length_x)+1:4*length(length_x)), "LineWidth",2)
xlabel(["","BR length"],'FontSize',15, "FontWeight","bold")
ylabel(["Fluid Surface",""],'FontSize',15, "FontWeight","bold")
title(["Bioreactor Media Surface Shape",""],'FontSize',15, "FontWeight","bold")
set(gca, "FontSize", 12)





























