# Centrifugal Bioreactor Project

This is a repository for all calculation and estimation scripts for BE-410-411's project, owned and maintained by Bio-ReacT team.

## Contributors

Our group has five members:

- Ian Steuk
- Kaylie Green
- Mason Summers
- Nhan Nguyen
- Zainab Alsaihati

## Directory Overview

- **Source Code Files**

| File Name                          | Purpose                                                                                                                   |
| ---------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| `br_mechanics.m`                   | Predict the mechanics of liquid inside the flask bioreactor during spinning. Plot a liquid surface shape after execution. |
| `cell_dynamics.m`                  | Premilinary cell growth dynamics, could be used to generate results for logistic and exponential cell growth.             |
| `plot_final_poster_presentation.m` | Generate a plot for final predicted versus experimental liquid uniformity's angle and speed.                              |

- **Image Files**

| File Name                              | Purpose                                                                                                     |
| -------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| `figure_3_final_poster_horizontal.png` | Horizontally arranged predicted versus experimental liquid uniformity's angle and speed plot.               |
| `figure_3_final_poster_vertical.png`   | Vertically arranged predicted versus experimental liquid uniformity's angle and speed plot.                 |
| `media_surface_shape.png`              | Shape of media surface during centrifuging.                                                                 |
| `plot_final_poster_presentation_2.png` | Predicted versus experimental liquid uniformity's angle and speed plot. Used for final poster presentation. |
| `pressure_distribution.png`            | Distribution of pressure along the horizontal axis with respect to each angle and speed.                    |
| `pressure_vs_angle_speed_1.png`        | Preliminary predicted pressure at the cell bed with respect to angle and speed.                             |
