%% BE_410_411 
% This script is used to 
% 1. make a pretty plot for the Advisory Board poster presentation 
% author: Nhan Nguyen 
% date: 12/28/2022 

close all, clc

% run the line below for only 5 seconds then hit stop
% br_mechanics 


% % some calculations 
% speeds = unique(uniform_speed); 
% 
% hold on 
% for i = 1:length(speeds)
%     angle = uniform_angle(uniform_speed == speeds(i))
%     boxchart(speeds(i)*ones(1,length(angle)), angle, 'orientation','horizontal')
% end

axes1 = axes('Parent',figure);

angles = unique(uniform_angle); 

hold on 
for i = 1:length(angles)
    speed = uniform_speed(uniform_angle == angles(i));
    plot1 = plot(angles(i)*ones(1,length(speed)), speed, "-", 'Color',[1 0 0.4], "LineWidth", 2);
end

gcf.MarkerStyle = "none";
gcf.LineWidth = 2;
gcf.WhiskerLineColor = "black";
xlim([5 50])
ylim([30 140])
xlabel(["","Angle (degree)"],'FontSize',15, "FontWeight","bold")
ylabel(["Centrifugal Speed (RPM)",""],'FontSize',15, "FontWeight","bold")
title(["Theoretical Range of Liquid Uniformity",""],'FontSize',15, "FontWeight","bold")
set(gca, "FontSize", 12)

real_angle = [20 25 30 35 40 45 ];
real_speed = [60 78 90 96 104 106];
plot2 = plot(real_angle, real_speed, "x", ...
    'Color',[0 0.2 1], "MarkerSize", 20, "LineWidth", 5, ...
    'DisplayName','Experimental Results');


legend1  = legend(plot1,'Location','NorthEast');
set(legend1,...
    'Position',[0.255793103448276 0.850590339892665 0.266206896551724 0.0330948121645795],...
    'FontSize',12,...
    'EdgeColor',[1 1 1]);
axis1 = axes('position',get(gca,'position'),'visible','off');
legend2 = legend(axis1, plot2,'Location',"NorthWest");
set(legend2,...
    'Position',[0.250591133004926 0.800833546298663 0.213103448275862 0.0330948121645795],...
    'FontSize',12,...
    'EdgeColor',[1 1 1]);


hold off 

% % plot 
% gscatter(uniform_angle, uniform_speed, uniform_speed);
% xlabel(["","Angle"],'FontSize',15, "FontWeight","bold")
% ylabel(["Centrifuge Speed",""],'FontSize',15, "FontWeight","bold")
% title(["Bioreactor Media Surface Shape",""],'FontSize',15, "FontWeight","bold")
% set(gca, "FontSize", 12)









